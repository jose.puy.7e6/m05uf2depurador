﻿using System;

namespace IsAdult
{
    public class Exercici1
    {
        static Boolean IsAdult(int todayYear, int todayMonth, int todayDay, int birthYear,
            int birthMonth, int birthDay)
        {
            var IsAdult = todayYear > birthYear + 18;
            var isDifficultYear = todayYear == birthYear + 18;
            var IsAdultForMonth = isDifficultYear && todayMonth > birthMonth;     // Li retirem el "=" de la condició de entre les variables "_Month"
            var isDifficultMonth = isDifficultYear && todayMonth == birthMonth;
            var IsAdultForDay = isDifficultMonth && todayDay >= birthDay;   // Li afegim el "=" de la condició de entre les variables "_Day"
            return IsAdult || IsAdultForMonth || IsAdultForDay;
        }

        static void Main()
        {
            if (!IsAdult(2018, 10, 17, 1985, 1, 1)) //true
            {
                Console.WriteLine("Ha fallat: IsAdult(2018, 10, 17, 1985, 1, 1)");
            }
            if (IsAdult(2018, 10, 17, 2015, 1, 1)) //false
            {
                Console.WriteLine("Ha fallat IsAdult(2018, 10, 17, 2015, 1, 1)"); 
            }
            if (!IsAdult(2018, 10, 17, 2000, 1, 1)) //true
            {
                Console.WriteLine("Ha fallat: IsAdult(2018, 10, 17, 2000, 1, 1)");
            }
            if (IsAdult(2018, 10, 17, 2000, 12, 1)) //false
            {
                Console.WriteLine("Ha fallat IsAdult(2018, 10, 17, 2000, 12, 1)");
            }
            if (!IsAdult(2018, 10, 17, 2000, 10, 1)) //true
            {
                Console.WriteLine("Ha fallat: IsAdult(2018, 10, 17, 2000, 10, 1)");
            }
            if (!IsAdult(2018, 10, 17, 2000, 10, 17)) //true
            {
                Console.WriteLine("Ha fallat: IsAdult(2018, 10, 17, 2000, 10, 17)");
            }
            if (IsAdult(2018, 10, 17, 2000, 10, 30)) //false
            {
                Console.WriteLine("Ha fallat IsAdult(2018, 10, 17, 2000, 10, 30)");
            }

            Console.ReadLine();
        }
    }
}
