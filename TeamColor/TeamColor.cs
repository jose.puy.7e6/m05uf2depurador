﻿using System;

namespace TeamColor
{
    class TeamColor
    {
        static void Main()
        {
            string color1 = Console.ReadLine();   // No fa falta convertir, ja que el propi ReadLine ja és un String.
            string color2 = Console.ReadLine();     // Canviem el nom de la segona variable de "color1" a "color2". Treiem el parentesis que sobra.
            if (color1 == "white")
            {
                if (color2 == "green") Console.WriteLine("Team1");           // Afegim el ; final, a més de corregir els dos "WriteLine".
                else if (color2 == "blue") Console.WriteLine("Team2");
                else if (color2 == "brown") Console.Write("Team3");              //Corregim les respostes per a que doni els equips correctes.
                else
                {
                    Console.WriteLine("ERROR");
                }
            }
            else if (color1 == "red")
            {
                if (color2 == "blue")
                {
                    Console.WriteLine("Team4");        // Afegim el ; final.
                }
                else if (color2 == "black")            
                {
                    Console.WriteLine("Team5");
                }
                else
                {
                    Console.WriteLine("ERROR");
                }
            }
            else if (color1 == "green")             // Convertim els "If" en "Else If".
            {
                if(color2 == "red")
                { 
                    Console.WriteLine("Team6");
                }
                else
                {
                    Console.WriteLine("ERROR");
                }
            }

            Console.ReadLine();
        }

    }
}
