﻿using System;

namespace Mates
{
    class Mates
    {
        static void Main()
        {
            var result = 0.0;
            for (int i = 0; i < 10000; i++)                  // Arreglem els errors d'indentació, punts i comes, i cometes.
            {
                if (result > 100)
                {
                    result = Math.Sqrt(result);             // Corregim la funció de l'arrel quadrada.
                }

                if (result < 0)
                {
                    result += result * result;
                }

                result += 20.2;
            }

            Console.WriteLine($"el resultat és {result}");

            Console.ReadLine();
        }
    }
}


/*
 * Quin valor té result quan la i == 1000?
 *  Quan la variable "i" és igual a "1000", la variable "result" és igual a "10.562305898749054"
 * El valor result és mai major que 110? I que 120? 
 *  Sempre arriba a ser major que "110", "111" per ser exactes. Però llavors és fa l'arrel quadrada i baixa el número. Per aquesta és impossible que el valor de "result" sigui "120", o superior.
 */
