﻿using System;

namespace DebugFor
{
    class DebugFor
    {
        static void Main()
        {
            var numero1 = 548745184;
            var numero2 = 25145;
            long result = 0;        //La solució era definir el tipus de variable "result" a "long", per tal de poder enmagatzemar el valor correcte de la multiplicació.
            for (int i = 0; i < numero2; i++)
            {
                result += numero1;
            }

            Console.WriteLine("la multiplicació de {0} i {1} es {2}", numero1, numero2, result);

            Console.ReadLine();
        }
    }
}
